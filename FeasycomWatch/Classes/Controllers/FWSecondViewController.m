//
//  FWSecondViewController.m
//  FeasycomWatch
//
//  Created by LIDONG on 14-2-28.
//  Copyright (c) 2014年 LIDONG. All rights reserved.
//

#import "FWSecondViewController.h"

@interface FWSecondViewController ()

@end

@implementation FWSecondViewController

- (id)init {
    self = [super init];
    if (self) {
        NSString *title = @"Notification Center";
        
        [self setTitle:title];
        
        UITabBarItem *tabBarItem = [[UITabBarItem alloc] initWithTabBarSystemItem:UITabBarSystemItemFeatured tag:2];
        
        [tabBarItem setTitle:title];
        [self setTabBarItem:tabBarItem];
        [tabBarItem release];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
