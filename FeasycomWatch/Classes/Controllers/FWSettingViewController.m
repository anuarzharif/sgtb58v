//
//  FWSettingViewController.m
//  FeasycomWatch
//
//  Created by LIDONG on 14-2-24.
//  Copyright (c) 2014年 LIDONG. All rights reserved.
//

#import "FWSettingViewController.h"

@interface FWSettingViewController ()

@end

@implementation FWSettingViewController

- (id)init {
    self = [super init];
    if (self) {
        NSString *title = @"Help";
        
        [self setTitle:title];
        
        UITabBarItem *tabBarItem = [[UITabBarItem alloc] initWithTabBarSystemItem:UITabBarSystemItemMore tag:3];
        
        [tabBarItem setTitle:title];
        [self setTabBarItem:tabBarItem];
        [tabBarItem release];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
