//
//  FWRootViewController.m
//  FeasycomWatch
//
//  Created by LIDONG on 14-2-28.
//  Copyright (c) 2014年 LIDONG. All rights reserved.
//

#import "FWRootViewController.h"
#import "FWCentralViewController.h"
#import "FWSecondViewController.h"
#import "FWSettingViewController.h"

@interface FWRootViewController () {
    
}

@end

@implementation FWRootViewController

- (id)init {
    self = [super initWithNibName:nil bundle:nil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
	NSMutableArray *viewControllers = [[NSMutableArray alloc] initWithCapacity:5];
	UIViewController *viewController;
	UINavigationController *navController;
    
#define ADD_VIEW_CONTROLLER(ViewControllerClass) do { \
viewController = [[ViewControllerClass alloc] init]; \
navController = [[UINavigationController alloc] initWithRootViewController:viewController]; \
[viewControllers addObject:navController]; \
[navController release]; \
[viewController release]; \
} while(0)
    
    ADD_VIEW_CONTROLLER(FWCentralViewController);
    ADD_VIEW_CONTROLLER(FWSecondViewController);
    ADD_VIEW_CONTROLLER(FWSettingViewController);
    
    [self setViewControllers:viewControllers];
    [viewControllers release];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation {
    return (UIInterfaceOrientationPortrait == toInterfaceOrientation);
}

- (BOOL)shouldAutorotate {
    return NO;
}

//- (NSUInteger)supportedInterfaceOrientations {
//    return UIInterfaceOrientationMaskPortrait;
//}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation {
    return UIInterfaceOrientationPortrait;
}

@end
