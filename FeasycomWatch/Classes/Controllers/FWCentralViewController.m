//
//  FWCentralViewController.m
//  FeasycomWatch
//
//  Created by LIDONG on 14-2-28.
//  Copyright (c) 2014年 LIDONG. All rights reserved.
//

#import "FWCentralViewController.h"
#import "FWSessionViewController.h"
#import "BFRefreshControl.h"
#import "FWPeripheralWrapper.h"
#import <CoreBluetooth/CoreBluetooth.h>

@interface FWCentralViewController () <CBCentralManagerDelegate, UITableViewDataSource, UITableViewDelegate, BFRefreshControlDelegate> {
    CBCentralManager *mCentralManager;
    CBPeripheral *mConnectedPeripheral;
    UITableView *mTableView;
    BFRefreshControl *mRefreshControl;
    NSMutableArray *mPeripheralWrappers;
    FWPeripheralWrapper *mSelectedPeripheralWrapper;
}

- (BOOL)startScanning;
- (void)stopScanning;
- (void)scanningDidFinish;

@end

@implementation FWCentralViewController

- (id)init {
    self = [super init];
    if (self) {
        [self setTitle:@"Feasycom Watch"];
        
        UITabBarItem *tabBarItem = [[UITabBarItem alloc] initWithTabBarSystemItem:UITabBarSystemItemSearch tag:1];
        
        [tabBarItem setTitle:@"WATCH"];
        [self setTabBarItem:tabBarItem];
        [tabBarItem release];
        
        mPeripheralWrappers = [[NSMutableArray alloc] init];
        mCentralManager = [[CBCentralManager alloc] initWithDelegate:self queue:NULL];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIView *view = [self view];
    const CGRect frame = [view frame];
    
    mTableView = [[UITableView alloc] initWithFrame:frame style:UITableViewStylePlain];
    [mTableView setDataSource:self];
    [mTableView setDelegate:self];
    [mTableView setRowHeight:100];
    [mTableView setBackgroundColor:[UIColor whiteColor]];
    [mTableView setClipsToBounds:NO];
    [mTableView setAutoresizingMask:UIViewAutoresizingNone];
    [mTableView setSeparatorStyle:UITableViewCellSeparatorStyleSingleLine];
    [mTableView setAllowsSelection:YES];
    
    mRefreshControl = [[BFRefreshControl alloc] initWithFrame:CGRectMake(0, -kRefreshControlHeight, frame.size.width, kRefreshControlHeight)];
    [mRefreshControl setBackgroundColor:[UIColor lightGrayColor]];
    [mRefreshControl setDelegate:self];
    [mTableView addSubview:mRefreshControl];
    
    [view addSubview:mTableView];
}

- (void)releaseUI {
    RELEASE_SAFELY(mTableView);
    RELEASE_SAFELY(mRefreshControl);
}

- (void)releaseOthers {
    [mCentralManager cancelPeripheralConnection:mConnectedPeripheral];
    [mCentralManager release];
    [mConnectedPeripheral release];
    [mPeripheralWrappers release];
}

- (void)deselect:(BOOL)animated {
    NSIndexPath *selectedIndexPath = [mTableView indexPathForSelectedRow];
    
    if (selectedIndexPath) {
        [mTableView deselectRowAtIndexPath:selectedIndexPath animated:animated];
    }
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    if (mConnectedPeripheral) {
        [mCentralManager cancelPeripheralConnection:mConnectedPeripheral];
        RELEASE_SAFELY(mConnectedPeripheral);
    }
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self deselect:animated];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self stopScanning];
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	return [mPeripheralWrappers count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath  {
    const NSInteger row = [indexPath row];
    static NSString *CellIdentifier = @"Cell";
	UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    UILabel *textLabel = nil;
    UILabel *detailTextLabel = nil;
	
	if (nil == cell) {
		cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier] autorelease];
        
        [cell setSelectionStyle:UITableViewCellSelectionStyleGray];
        [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
        textLabel = [cell textLabel];
        detailTextLabel = [cell detailTextLabel];
        [detailTextLabel setNumberOfLines:3];
	} else {
        textLabel = [cell textLabel];
        detailTextLabel = [cell detailTextLabel];
    }
    
    FWPeripheralWrapper *peripheralWrapper = [mPeripheralWrappers objectAtIndex:row];
    
    [textLabel setText:[NSString stringWithFormat:@"%@ (RSSI: %d)", [peripheralWrapper name], (int)[peripheralWrapper RSSI]]];
    [detailTextLabel setText:[NSString stringWithFormat:@"%@\n%@", [peripheralWrapper UUIDString], [peripheralWrapper advertisementData]]];
    
	return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
//    [self startWaitingWithTitle:LS(@"CONNECTING")];
    [self startWaitingWithTitle:@"Connecting"];
    
    CBPeripheral *peripheral = [[mPeripheralWrappers objectAtIndex:[indexPath row]] peripheral];
    
    [mCentralManager connectPeripheral:peripheral options:nil];
}

- (void)updateData {
    if ([self startScanning]) {
        [mRefreshControl beginRefreshing:mTableView];
    } else {
        [mRefreshControl stopRefreshing:mTableView];
        [mPeripheralWrappers removeAllObjects];
        [mTableView reloadData];
        ALERT(@"BLUETOOTH_POWEROFF_TITLE", @"BLUETOOTH_POWEROFF_PROMPT");
    }
}

#pragma mark - BFRefreshControlDelegate

- (void)refreshControlDidBeginRefresh:(BFRefreshControl *)refreshControl {
    [self updateData];
}

#pragma mark - CBCentralManagerDelegate

- (void)centralManagerDidUpdateState:(CBCentralManager *)central {
    DLog(@"state: %d", [central state]);
    [self updateData];
}

- (void)centralManager:(CBCentralManager *)central didDiscoverPeripheral:(CBPeripheral *)peripheral advertisementData:(NSDictionary *)advertisementData RSSI:(NSNumber *)RSSI {
    FWPeripheralWrapper *peripheralWrapper = [[FWPeripheralWrapper alloc] initWithPeripheral:peripheral RSSI:RSSI advertisementData:advertisementData];
    
    DLog(@"NAME: %@\nUUID: %@\nRSSI: %@", [peripheral name], [peripheralWrapper UUIDString], RSSI);
    
    if (![mPeripheralWrappers containsObject:peripheralWrapper]) {
        const NSInteger newIndex = [mPeripheralWrappers count];
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:newIndex inSection:0];
        
        [mPeripheralWrappers insertObject:peripheralWrapper atIndex:newIndex];
        [mTableView insertRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationLeft];
        
        if (0 == newIndex) {
            [self performSelector:@selector(scanningDidFinish) withObject:nil afterDelay:3];
        } else if (10 == newIndex) {
            [self performSelector:@selector(scanningDidFinish) withObject:nil afterDelay:0.5];
        }
    }
    [peripheralWrapper release];
}

- (void)centralManager:(CBCentralManager *)central didConnectPeripheral:(CBPeripheral *)peripheral {
    RETAIN_SAFELY(mConnectedPeripheral, peripheral);
    
    NSString *name = [mConnectedPeripheral name];
    if (nil == name) {
        name = LS(@"UNNAMED");
    }
    FWSessionViewController *viewController = [[FWSessionViewController alloc] initWithPeripheral:mConnectedPeripheral centralManager:mCentralManager];
    
    [viewController setTitle:name];
    [[self navigationController] pushViewController:viewController animated:YES];
    [viewController release];
    
    [self stopWaiting];
}

- (void)centralManager:(CBCentralManager *)central didFailToConnectPeripheral:(CBPeripheral *)peripheral error:(NSError *)error {
    [self stopWaitingWithTitle:[error localizedDescription]];
}

- (void)centralManager:(CBCentralManager *)central didDisconnectPeripheral:(CBPeripheral *)peripheral error:(NSError *)error {
    [[self navigationController] popToRootViewControllerAnimated:YES];
    [self alertWithTitle:LS(@"CONNECTION_ABORT")];
}

#pragma mark -

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    [mRefreshControl scrollViewDidScroll:scrollView];
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    [mRefreshControl scrollViewDidEndDragging:scrollView];
}

#pragma mark - Actions

- (BOOL)startScanning {
    if (CBCentralManagerStatePoweredOn == [mCentralManager state]) {
        [mPeripheralWrappers removeAllObjects];
        [mTableView reloadData];
        [mCentralManager scanForPeripheralsWithServices:nil options:@{ CBCentralManagerScanOptionAllowDuplicatesKey : @YES }];
        [self performSelector:@selector(sacnningDidTimeout) withObject:nil afterDelay:10];
        return YES;
    }
    return NO;
}

- (void)stopScanning {
    [NSObject cancelPreviousPerformRequestsWithTarget:self];
    [mCentralManager stopScan];
    [mRefreshControl stopRefreshing:mTableView];
}

- (void)scanningDidFinish {
    [self stopScanning];
}

- (void)sacnningDidTimeout {
    [self stopScanning];
}

@end
