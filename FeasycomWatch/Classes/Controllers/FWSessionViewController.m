//
//  FWSessionViewController.m
//  FeasycomWatch
//
//  Created by LIDONG on 11/2/14.
//  Copyright (c) 2014 LIDONG. All rights reserved.
//

#import "FWSessionViewController.h"
#import <CoreBluetooth/CoreBluetooth.h>

const UIViewAnimationCurve GTKeyboardAnimationCurve = 7;
const NSTimeInterval GTKeyboardAnimationDuration = 0.25;

@interface FWSessionViewController () <UITextFieldDelegate, CBPeripheralDelegate> {
    UIView *mContentView;
    UILabel *mInStatisticsLabel;
    UITextView *mInTextView;
    UILabel *mOutStatisticsLabel;
    UITextField *mOutTextField;
    UITextField *mTimeTextField;
    UISwitch *mTimeSwitch;
    UIButton *mSendButton;
    UITextField *mActiveTextField;
    NSTimer *mTimer;
    CBCentralManager *mCentralManager;
    CBPeripheral *mConnectedPeripheral;
    CBCharacteristic *mNotifingCharacteristic;
    CBCharacteristic *mTransferCharacteristic;
    NSMutableData *mCachingData;
    NSMutableString *mReceivedText;
    NSData *mSendingData;
    int mReceivedBytes;
    int mSentBytes;
    int mReceivedCount;
    int mSentCount;
    CGFloat mKeyboardHeight;
}

- (void)updateStatistics;
- (void)cleanup;

@end

@implementation FWSessionViewController

- (void)releaseUI {
    [super releaseUI];
    RELEASE_SAFELY(mContentView);
    RELEASE_SAFELY(mInStatisticsLabel);
    RELEASE_SAFELY(mInTextView);
    RELEASE_SAFELY(mOutStatisticsLabel);
    RELEASE_SAFELY(mOutTextField);
    RELEASE_SAFELY(mTimeTextField);
    RELEASE_SAFELY(mTimeSwitch);
    RELEASE_SAFELY(mSendButton);
}

- (void)releaseOthers {
    [self cleanup];
    
    [mCentralManager release];
    [mConnectedPeripheral release];
    [mNotifingCharacteristic release];
    [mTransferCharacteristic release];
    [mCachingData release];
    [mReceivedText release];
    [mSendingData release];
    
    [super releaseOthers];
}

- (id)initWithPeripheral:(CBPeripheral *)peripheral centralManager:(CBCentralManager *)centralManager {
    if (self = [super init]) {
        [self setHidesBottomBarWhenPushed:YES];
        mKeyboardHeight = kDefaultKeyboardHeight;
        
        mCentralManager = [centralManager retain];
        
        mConnectedPeripheral = [peripheral retain];
        [mConnectedPeripheral setDelegate:self];
        [mConnectedPeripheral discoverServices:nil];
        
        mReceivedText = [[NSMutableString alloc] init];
        mCachingData = [[NSMutableData alloc] initWithCapacity:4096];
        
        UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Clear" style:UIBarButtonItemStyleBordered target:self action:@selector(onClear)];
        
        [[self navigationItem] setRightBarButtonItem:barButtonItem];
        
        [barButtonItem release];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    if (nil == mContentView) {
        UIColor *clearColor = [UIColor clearColor];
        UIColor *textColor = [UIColor darkTextColor];
        UIFont *font = [UIFont systemFontOfSize:16];
        const CGFloat contentHeight = kScreenHeight - kDefaultStatusBarHeight - kNavBarHeight;
        const CGFloat contentWidth = kScreenWidth - 10;
        
        mContentView = [[UIView alloc] initWithFrame:(CGRect){ 0, 0, kScreenWidth, contentHeight }];
        [mContentView setBackgroundColor:[UIColor whiteColor]];
        [mContentView setAutoresizesSubviews:NO];
        
        mInStatisticsLabel = [[UILabel alloc] initWithFrame:CGRectMake(5, 5, contentWidth, 20)];
        [mInStatisticsLabel setTextAlignment:NSTextAlignmentLeft];
        [mInStatisticsLabel setBackgroundColor:clearColor];
        [mInStatisticsLabel setTextColor:textColor];
        [mInStatisticsLabel setFont:font];
        [mContentView addSubview:mInStatisticsLabel];
        
        const CGFloat outOffsetY = contentHeight - 100;
        const CGFloat inTextHeight = outOffsetY - 45;
        
        mInTextView = [[UITextView alloc] initWithFrame:CGRectMake(5, 30, contentWidth, inTextHeight)];
        [mInTextView setBackgroundColor:[UIColor lightGrayColor]];
        [mInTextView setTextColor:textColor];
        [mInTextView setFont:font];
        [mInTextView setEditable:NO];
        [mInTextView setKeyboardAppearance:UIKeyboardAppearanceDefault];
        [mInTextView setKeyboardType:UIKeyboardTypeDefault];
        [mInTextView setAutocapitalizationType:UITextAutocapitalizationTypeNone];
        [mInTextView setAutocorrectionType:UITextAutocorrectionTypeNo];
        [mInTextView setReturnKeyType:UIReturnKeyNext];
        [mInTextView setText:mReceivedText];
        [mContentView addSubview:mInTextView];
        
        mOutStatisticsLabel = [[UILabel alloc] initWithFrame:CGRectMake(5, outOffsetY, contentWidth, 20)];
        [mOutStatisticsLabel setTextAlignment:NSTextAlignmentLeft];
        [mOutStatisticsLabel setBackgroundColor:clearColor];
        [mOutStatisticsLabel setTextColor:textColor];
        [mOutStatisticsLabel setFont:font];
        [mContentView addSubview:mOutStatisticsLabel];
        
        mOutTextField = [[UITextField alloc] initWithFrame:CGRectMake(5, outOffsetY + 25, contentWidth, 31)];
        [mOutTextField setBackgroundColor:clearColor];
        [mOutTextField setTextColor:textColor];
        [mOutTextField setDelegate:self];
        [mOutTextField setBorderStyle:UITextBorderStyleRoundedRect];
        [mOutTextField setContentVerticalAlignment:UIControlContentVerticalAlignmentCenter];
        [mOutTextField setFont:font];
        [mOutTextField setKeyboardAppearance:UIKeyboardAppearanceDefault];
        [mOutTextField setKeyboardType:UIKeyboardTypeASCIICapable];
        [mOutTextField setAutocapitalizationType:UITextAutocapitalizationTypeNone];
        [mOutTextField setAutocorrectionType:UITextAutocorrectionTypeNo];
        [mOutTextField setReturnKeyType:UIReturnKeySend];
        [mOutTextField setClearButtonMode:UITextFieldViewModeWhileEditing];
        [mContentView addSubview:mOutTextField];
        
        const CGFloat offsetY = outOffsetY + 60;
        
        UILabel *label = [[UILabel alloc] initWithFrame:(CGRect){ 5, offsetY, 65, 31 }];
        
        [label setBackgroundColor:clearColor];
        [label setTextColor:textColor];
        [label setFont:font];
        [label setText:@"Timed transmission"];
        [mContentView addSubview:label];
        [label release];
        
        mTimeSwitch = [[UISwitch alloc] initWithFrame:(CGRect){ 70, offsetY, 53, 31 }];
        [mTimeSwitch addTarget:self action:@selector(onValueChanged:) forControlEvents:UIControlEventValueChanged];
        [mContentView addSubview:mTimeSwitch];
        
        mTimeTextField = [[UITextField alloc] initWithFrame:CGRectMake(140, offsetY, 80, 31)];
        [mTimeTextField setBackgroundColor:clearColor];
        [mTimeTextField setTextColor:textColor];
        [mTimeTextField setTextAlignment:NSTextAlignmentRight];
        [mTimeTextField setDelegate:self];
        [mTimeTextField setBorderStyle:UITextBorderStyleRoundedRect];
        [mTimeTextField setContentVerticalAlignment:UIControlContentVerticalAlignmentCenter];
        [mTimeTextField setFont:font];
        [mTimeTextField setKeyboardAppearance:UIKeyboardAppearanceDefault];
        [mTimeTextField setKeyboardType:UIKeyboardTypeNumberPad];
        [mTimeTextField setAutocapitalizationType:UITextAutocapitalizationTypeNone];
        [mTimeTextField setAutocorrectionType:UITextAutocorrectionTypeNo];
        [mTimeTextField setReturnKeyType:UIReturnKeyDone];
        [mTimeTextField setClearButtonMode:UITextFieldViewModeNever];
        [mTimeTextField setEnabled:[mTimeSwitch isOn]];
        [mContentView addSubview:mTimeTextField];
        
        label = [[UILabel alloc] initWithFrame:(CGRect){ 220, offsetY, 32, 31 }];
        
        [label setBackgroundColor:clearColor];
        [label setTextAlignment:NSTextAlignmentRight];
        [label setTextColor:textColor];
        [label setText:@"ms"];
        [mContentView addSubview:label];
        [label release];
        
        mSendButton = [[UIButton buttonWithType:UIButtonTypeRoundedRect] retain];
        
        [mSendButton setFrame:CGRectMake(kScreenWidth - 65, offsetY, 60, 31)];
        [mSendButton.titleLabel setFont:[UIFont boldSystemFontOfSize:16]];
        [mSendButton setTitleColor:textColor forState:UIControlStateNormal];
        [mSendButton setTitle:@"Send" forState:UIControlStateNormal];
        [mSendButton addTarget:self action:@selector(startWriting) forControlEvents:UIControlEventTouchUpInside];
        [mContentView addSubview:mSendButton];
        
        [self updateStatistics];
        
        UIView *separatorView = [[UIView alloc] initWithFrame:(CGRect){ 0, contentHeight - 1, kScreenWidth, 1 }];
        
        [separatorView setBackgroundColor:[UIColor colorWithWhite:0.85 alpha:1.f]];
        [mContentView addSubview:separatorView];
        [separatorView release];
    }
    
    [[self view] setUserInteractionEnabled:YES];
    [[self view] addSubview:mContentView];
}

- (void)stopWriting {
    if (mTimer) {
        [mSendButton setTitle:@"Send" forState:UIControlStateNormal];
        STOP_TIMER(mTimer);
    }
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self stopWriting];
}

- (void)updateControlStates {
    const BOOL enabled = (nil == mTimer);
    
    [mOutTextField setEnabled:enabled];
    [mTimeSwitch setEnabled:enabled];
    [mTimeTextField setEnabled:(enabled && [mTimeSwitch isOn])];
}

- (void)onValueChanged:(UISwitch *)sender {
    const BOOL isOn = [mTimeSwitch isOn];
    
    if (!isOn) {
        [self stopWriting];
    }
    [mTimeTextField setEnabled:isOn];
}

- (void)writeData {
    [mConnectedPeripheral writeValue:mSendingData forCharacteristic:mTransferCharacteristic type:CBCharacteristicWriteWithoutResponse];
    mSentBytes += [mSendingData length];
    mSentCount ++;
    [self updateStatistics];
}

- (void)startWriting {
    if (mTimer) {
        [self stopWriting];
        return;
    }
    NSString *text = [mOutTextField text];
    
    if ([text length] > 0) {
        /*
        while ([text lengthOfBytesUsingEncoding:NSUTF8StringEncoding] >= 20) {
            text = [text substringToIndex:[text length] - 1];
        }*/
        text = [text stringByAppendingString:@"\r"];
        
        RETAIN_SAFELY(mSendingData, [text dataUsingEncoding:NSUTF8StringEncoding]);
        
        if ([mTimeSwitch isOn]) {
            if (nil == mTimer) {
                [mSendButton setTitle:@"Stop" forState:UIControlStateNormal];
                mTimer = [NSTimer scheduledTimerWithTimeInterval:([[mTimeTextField text] doubleValue] / 1000.f) target:self selector:@selector(writeData) userInfo:nil repeats:YES];
            }
        } else {
            [self writeData];
        }
        
        [mOutTextField resignFirstResponder];
    }
}

#pragma mark -


/** The Transfer Service was discovered
 */
- (void)peripheral:(CBPeripheral *)peripheral didDiscoverServices:(NSError *)error
{
    if (error) {
        DLog(@"Error discovering services: %@", [error localizedDescription]);
        [self cleanup];
        return;
    }
    
    // Discover the characteristic we want...
    
    // Loop through the newly filled peripheral.services array, just in case there's more than one.
    for (CBService *service in peripheral.services) {
        
        DLog(@"%@", [[[NSString alloc] initWithData:service.UUID.data encoding:NSUTF8StringEncoding] autorelease]);
        [peripheral discoverCharacteristics:nil forService:service];
    }
}


/** The Transfer characteristic was discovered.
 *  Once this has been found, we want to subscribe to it, which lets the peripheral know we want the data it contains
 */
- (void)peripheral:(CBPeripheral *)peripheral didDiscoverCharacteristicsForService:(CBService *)service error:(NSError *)error
{
    // Deal with errors (if any)
    if (error) {
        DLog(@"Error discovering characteristics: %@", [error localizedDescription]);
        [self cleanup];
        return;
    }
    
    // Again, we loop through the array, just in case.
    for (CBCharacteristic *characteristic in service.characteristics) {
        
        const CBCharacteristicProperties properties = [characteristic properties];
        
        DLog(@"%@ peroperties: %x", characteristic, (int)properties);
        
        // And check if it's the right one
        if (CBCharacteristicPropertyNotify & properties) {
            // If it is, subscribe to it
            if (characteristic != mNotifingCharacteristic) {
                if (mNotifingCharacteristic) {
                    [peripheral setNotifyValue:NO forCharacteristic:mNotifingCharacteristic];
                    [mNotifingCharacteristic release];
                }
                mNotifingCharacteristic = [characteristic retain];
                [peripheral setNotifyValue:YES forCharacteristic:mNotifingCharacteristic];
            }
        }
        if ((CBCharacteristicPropertyWriteWithoutResponse & properties) || (CBCharacteristicPropertyWrite & properties)) {
            if (characteristic != mTransferCharacteristic) {
                RETAIN_SAFELY(mTransferCharacteristic, characteristic);
            }
        }
    }
    
    // Once this is complete, we just need to wait for the data to come in.
}


/** This callback lets us know more data has arrived via notification on the characteristic
 */
- (void)peripheral:(CBPeripheral *)peripheral didUpdateValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error
{
    if (error) {
        DLog(@"Error discovering characteristics: %@", [error localizedDescription]);
        return;
    }
    
    NSData *value = [characteristic value];
    
    // Otherwise, just add the data on to what we already have
    [mCachingData appendData:value];
    
    const NSUInteger cachingLength = [mCachingData length];
    
    if (0 < cachingLength) {
        NSString *stringFromData = [[NSString alloc] initWithData:mCachingData encoding:NSUTF8StringEncoding];
        
        if (0 < [stringFromData length]) {
            [mCachingData setLength:0];
            mReceivedBytes += cachingLength;
            mReceivedCount ++;
            
            if (10240 <= mReceivedBytes) {
                [mReceivedText setString:stringFromData];
            } else {
                [mReceivedText appendString:stringFromData];
            }
            [mInTextView setText:mReceivedText];
            [mInTextView scrollRangeToVisible:NSMakeRange([mReceivedText length] - 1, 1)];
            
            [self updateStatistics];
        }
        
        [stringFromData release];
    }
    
    // Log it
    DLog(@"Received: %@", value);
}


/** The peripheral letting us know whether our subscribe/unsubscribe happened or not
 */
- (void)peripheral:(CBPeripheral *)peripheral didUpdateNotificationStateForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error {
    if (error) {
        DLog(@"Error changing notification state: %@", error.localizedDescription);
    }
    
    // Notification has started
    if (characteristic.isNotifying) {
        DLog(@"Notification began on %@", characteristic);
    }
    
    // Notification has stopped
    else {
        // so disconnect from the peripheral
        DLog(@"Notification stopped on %@.  Disconnecting", characteristic);
        [self cleanup];
    }
}

/** Call this when things either go wrong, or you're done with the connection.
 *  This cancels any subscriptions if there are any, or straight disconnects if not.
 *  (didUpdateNotificationStateForCharacteristic will cancel the connection if a subscription is involved)
 */
- (void)cleanup {
    [mConnectedPeripheral setNotifyValue:NO forCharacteristic:mNotifingCharacteristic];
    
    // If we've got this far, we're connected, but we're not subscribed, so we just disconnect
    [mCentralManager cancelPeripheralConnection:mConnectedPeripheral];
    [[self navigationController] popViewControllerAnimated:YES];
}

- (NSString *)stringFromBytes:(NSInteger)bytes {
    if (1024 * 1024 <= bytes) {
        return [NSString stringWithFormat:@"%.2fMB", (CGFloat)bytes / (CGFloat)(1024 * 1024)];
    } else if (1024 <= bytes) {
        return [NSString stringWithFormat:@"%.2fKB", (CGFloat)bytes / 1024.f];
    }
    return [NSString stringWithFormat:@"%uB", (unsigned)bytes];
}

- (void)updateStatistics {
    [mInStatisticsLabel setText:[NSString stringWithFormat:@"Receive：%dbyte %dPackets", mReceivedBytes, mReceivedCount]];
    [mOutStatisticsLabel setText:[NSString stringWithFormat:@"Send：%dbyte %dPackets", mSentBytes, mSentCount]];
}

- (void)calculateLayout {
    CGRect frame = { 0, 0, kScreenWidth, kScreenHeight - kDefaultStatusBarHeight - kNavBarHeight };
    
    if (mActiveTextField) {
        frame.origin.y -= mKeyboardHeight;
    }
    [mContentView setFrame:frame];
}

- (void)onClear {
    [mCachingData setLength:0];
    [mReceivedText setString:@""];
    [mInTextView setText:mReceivedText];
    mReceivedBytes = 0;
    mReceivedCount = 0;
    mSentBytes = 0;
    mSentCount = 0;
    [self updateStatistics];
}

#pragma mark - UITextViewFieldDelegate

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if (textField == mTimeTextField) {
        if ([string length] > 0 && ![string isEqualToString:@"0"] && [string intValue] < 1) {
            return NO;
        }
    }
    return YES;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    DLog(@"%s", __PRETTY_FUNCTION__);
    UITextField *previousTextField = mActiveTextField;
    
    mActiveTextField = textField;
    
    if (nil == previousTextField) {
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationCurve:GTKeyboardAnimationCurve];
        [UIView setAnimationDuration:GTKeyboardAnimationDuration];
        [self calculateLayout];
        [UIView commitAnimations];
    }
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    DLog(@"%s", __PRETTY_FUNCTION__);
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField {
    DLog(@"%s", __PRETTY_FUNCTION__);
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    DLog(@"%s", __PRETTY_FUNCTION__);
    if (textField == mActiveTextField) {
        mActiveTextField = nil;
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationCurve:GTKeyboardAnimationCurve];
        [UIView setAnimationDuration:GTKeyboardAnimationDuration];
        [self calculateLayout];
        [UIView commitAnimations];
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    
    if (textField == mOutTextField) {
        [self startWriting];
    }
    return YES;
}

#pragma mark - Notification

- (void)keyboardWillChangeFrame:(NSNotification *)notification {
    NSDictionary *userInfo = [notification userInfo];
    NSNumber *rectNumber = [userInfo objectForKey:@"UIKeyboardBoundsUserInfoKey"];
    
    if (rectNumber) {
        const CGFloat newHeight = rectNumber.CGRectValue.size.height;
        
        if (fabs(newHeight - mKeyboardHeight) >= 0.5f) {
            mKeyboardHeight = newHeight;
            
            if (mActiveTextField) {
                [self calculateLayout];
            }
        }
    }
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [mActiveTextField resignFirstResponder];
}

@end
