//
//  FWSessionViewController.h
//  FeasycomWatch
//
//  Created by LIDONG on 11/2/14.
//  Copyright (c) 2014 LIDONG. All rights reserved.
//

#import "BFViewController.h"

@class CBCentralManager;
@class CBPeripheral;

@interface FWSessionViewController : BFViewController

- (id)initWithPeripheral:(CBPeripheral *)peripheral centralManager:(CBCentralManager *)centralManager;

@end
