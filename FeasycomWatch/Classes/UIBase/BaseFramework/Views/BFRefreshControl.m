//
//  BFRefreshControl.m
//  FeasycomWatch
//
//  Created by LIDONG on 14-1-7.
//  Copyright (c) 2014年 LIDONG. All rights reserved.
//

#import "BFRefreshControl.h"
#import "FWPeripheralWrapper.h"

#define TEXT_COLOR	 [UIColor colorWithRed:87.f/255.f green:108.f/255.f blue:137.f/255.f alpha:1.f]
#define FLIP_ANIMATION_DURATION 0.18f


typedef enum __BFRefreshControlState {
	BFRefreshControlStateNormal = 0,
	BFRefreshControlStatePulling,
	BFRefreshControlStateLoading
} BFRefreshControlState;


@interface BFRefreshControl () {
	UILabel *_dateLabel;
	UILabel *_statusLabel;
	CALayer *_arrowImage;
	UIActivityIndicatorView *_activityIndicatorView;
    CGFloat _draggingOffsetY;
	BFRefreshControlState _state;
    NSTimeInterval _lastRefreshTime;
    NSTimeInterval _lastUpdateTime;
	id<BFRefreshControlDelegate> _delegate;
}

- (void)updateState:(BFRefreshControlState)aState;
- (BOOL)doBeginRefreshing:(UIScrollView *)scrollView;

@end

@implementation BFRefreshControl

@synthesize delegate = _delegate;
@synthesize lastUpdateTime = _lastUpdateTime;

- (id)initWithFrame:(CGRect)frame {
    return [self initWithFrame:frame arrowImageName:@"blueArrow.png" textColor:TEXT_COLOR];
}

- (id)initWithFrame:(CGRect)frame arrowImageName:(NSString *)arrow textColor:(UIColor *)textColor {
    if (self = [super initWithFrame:frame]) {
        UIColor *clearColor = [UIColor clearColor];
        UIColor *shadowColor = [UIColor colorWithWhite:0.9f alpha:1.f];
        const CGSize shadowOffset = { 0.5f, 1.f };
        
        [self setBackgroundColor:clearColor];
        [self setAutoresizingMask:UIViewAutoresizingFlexibleWidth];
        
		_statusLabel = [[UILabel alloc] initWithFrame:CGRectMake(0.f, frame.size.height - 50.f, self.frame.size.width, 18.f)];
		[_statusLabel setAutoresizingMask:UIViewAutoresizingFlexibleWidth];
		[_statusLabel setFont:[UIFont boldSystemFontOfSize:14.f]];
		[_statusLabel setTextColor:textColor];
		[_statusLabel setShadowColor:shadowColor];
		[_statusLabel setShadowOffset:shadowOffset];
		[_statusLabel setBackgroundColor:clearColor];
		[_statusLabel setTextAlignment:NSTextAlignmentCenter];
		[self addSubview:_statusLabel];
		
		_dateLabel = [[UILabel alloc] initWithFrame:CGRectMake(0.f, frame.size.height - 24.f, frame.size.width, 14.f)];
		[_dateLabel setAutoresizingMask:UIViewAutoresizingFlexibleWidth];
		[_dateLabel setFont:[UIFont systemFontOfSize:12.f]];
		[_dateLabel setTextColor:textColor];
		[_dateLabel setShadowColor:shadowColor];
		[_dateLabel setShadowOffset:shadowOffset];
		[_dateLabel setBackgroundColor:clearColor];
		[_dateLabel setTextAlignment:NSTextAlignmentCenter];
		[self addSubview:_dateLabel];
		
		_arrowImage = [[CALayer alloc] init];
		[_arrowImage setFrame:CGRectMake(25.f, frame.size.height - kRefreshControlHeight, 30.f, 55.f)];
		[_arrowImage setContentsGravity:kCAGravityResizeAspect];
		[_arrowImage setContents:(id)[[UIImage imageNamed:arrow] CGImage]];
        [_arrowImage setContentsScale:FWScreenScale];
		
		[[self layer] addSublayer:_arrowImage];
		
		_activityIndicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        
        [_activityIndicatorView setCenter:CGPointMake(40.f, frame.size.height / 2.f)];
		[self addSubview:_activityIndicatorView];
        
        _lastRefreshTime = 0.f;
        _lastUpdateTime = 0.f;
    }
	
    return self;
}

- (void)dealloc  {
    [_dateLabel release];
    [_statusLabel release];
    [_arrowImage release];
    [_activityIndicatorView release];
    
    [super dealloc];
}

- (void)willMoveToSuperview:(UIView *)newSuperview {
    if (newSuperview) {
		[self updateState:_state];
    }
}

- (void)setLastUpdateTime:(NSTimeInterval)lastUpdateTime {
    if ((lastUpdateTime - _lastUpdateTime) >= 1.f) {
        _lastUpdateTime = lastUpdateTime;
        
        NSDate *date = [[NSDate alloc] initWithTimeIntervalSince1970:_lastUpdateTime];
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        
        [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        
        [_dateLabel setText:[LS(@"LAST_UPDATED_TITLE") stringByAppendingString:[formatter stringFromDate:date]]];
        [formatter release];
        [date release];
    }
}

- (void)updateState:(BFRefreshControlState)state {
    DLog(@"%s STATE: %d", __PRETTY_FUNCTION__, state);
    if (BFRefreshControlStateNormal == state) {
        [_statusLabel setText:LS(@"PULL_DOWN_TO_REFRESH")];
        [_activityIndicatorView stopAnimating];
        
        [CATransaction begin];
        [CATransaction setAnimationDuration:FLIP_ANIMATION_DURATION];
        [CATransaction setValue:(id)kCFBooleanFalse forKey:kCATransactionDisableActions];
        [_arrowImage setHidden:NO];
        [_arrowImage setTransform:CATransform3DIdentity];
        [CATransaction commit];
        
    } else if (BFRefreshControlStatePulling == state) {
        [_statusLabel setText:LS(@"RELEASE_TO_REFRESH")];
        
        [CATransaction begin];
        [CATransaction setAnimationDuration:FLIP_ANIMATION_DURATION];
        [_arrowImage setTransform:CATransform3DMakeRotation(M_PI, 0.f, 0.f, 1.f)];
        [CATransaction commit];
        
    } else if (BFRefreshControlStateLoading == state) {
        [_statusLabel setText:LS(@"REFRESHING")];
        [_activityIndicatorView startAnimating];
        
        [CATransaction begin];
        [CATransaction setAnimationDuration:FLIP_ANIMATION_DURATION];
        [CATransaction setValue:(id)kCFBooleanTrue forKey:kCATransactionDisableActions];
        [_arrowImage setHidden:YES];
        [CATransaction commit];
        
    } else {
        return;
    }
    
	_state = state;
}

- (BOOL)doBeginRefreshing:(UIScrollView *)scrollView {
    const NSTimeInterval currentTime = NSGetCurrentTimestamp();
    const NSTimeInterval interval = currentTime - _lastRefreshTime;
    
    if (1.5f <= interval) {
        _lastRefreshTime = currentTime;
        [scrollView setContentInset:UIEdgeInsetsMake(kRefreshControlHeight, 0.f, 0.f, 0.f)];
        [self updateState:BFRefreshControlStateLoading];
        return YES;
    } else {
        [self updateState:BFRefreshControlStateNormal];
    }
    return NO;
}

- (void)beginRefreshing:(UIScrollView *)scrollView {
    if (BFRefreshControlStateNormal == _state) {
        _state = BFRefreshControlStatePulling;
        [UIView animateWithDuration:0.25 animations:^{
            [scrollView setContentOffset:CGPointMake(0.f, -kRefreshControlHeight)];
        } completion:^(BOOL finished) {
            if (finished && BFRefreshControlStatePulling == _state) {
                [self doBeginRefreshing:scrollView];
            }
        }];
    }
}

- (void)stopRefreshing:(UIScrollView *)scrollView {
    if (BFRefreshControlStateNormal != _state) {
        [self updateState:BFRefreshControlStateNormal];
        [UIView animateWithDuration:0.25 animations:^{
            [scrollView setContentInset:UIEdgeInsetsZero];
        } completion:^(BOOL finished) {
            if (BFRefreshControlStateNormal == _state) {
                [scrollView setContentOffset:CGPointZero animated:finished];
            }
        }];
    }
}

- (void)updateLastUpdateTime {
    [self setLastUpdateTime:NSGetCurrentTimestamp()];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if ([scrollView isDragging]) {
        const int offsetY = scrollView.contentOffset.y;
        
		if (BFRefreshControlStatePulling == _state && offsetY > -kRefreshControlHeight && offsetY > _draggingOffsetY) {
			[self updateState:BFRefreshControlStateNormal];
		}  else if (BFRefreshControlStateNormal == _state && offsetY <= -kRefreshControlHeight && offsetY < _draggingOffsetY) {
			[self updateState:BFRefreshControlStatePulling];
		}
        _draggingOffsetY = offsetY;
	}
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView {
	if (BFRefreshControlStatePulling == _state && scrollView.contentOffset.y <= -kRefreshControlHeight) {
        if ([self doBeginRefreshing:scrollView]) {
            if ([_delegate respondsToSelector:@selector(refreshControlDidBeginRefresh:)]) {
                [_delegate refreshControlDidBeginRefresh:self];
            }
        }
	}
}

@end
