//
//  BFActivityView.h
//  LIDONG
//
//  Created by LiDong on 13-7-12.
//  Copyright (c) 2013年 LIDONG. All rights reserved.
//

#import <UIKit/UIKit.h>

@class BFActivityView;

@protocol BFActivityViewDelegate <NSObject>
- (void)activityViewDidDismiss:(BFActivityView *)activityView;
@end

@interface BFActivityView : UIView

@property (nonatomic, assign) id<BFActivityViewDelegate> delegate;
@property (nonatomic, assign) BOOL hidesBackground;

- (void)showWithTitle:(NSString *)title;
- (void)dismiss;
- (void)dismissWithTitle:(NSString *)title;
- (void)detachDelegate:(id)aDelegate;

@end
