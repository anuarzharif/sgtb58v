//
//  BFRefreshControl.h
//  FeasycomWatch
//
//  Created by LIDONG on 14-1-7.
//  Copyright (c) 2014年 LIDONG. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>


@class BFRefreshControl;

@protocol BFRefreshControlDelegate <NSObject>
- (void)refreshControlDidBeginRefresh:(BFRefreshControl *)refreshControl;
@end

#define kRefreshControlHeight 65

@interface BFRefreshControl : UIView {
}

@property (nonatomic, assign) id<BFRefreshControlDelegate> delegate;
@property (nonatomic, assign) NSTimeInterval lastUpdateTime;

- (id)initWithFrame:(CGRect)frame arrowImageName:(NSString *)arrow textColor:(UIColor *)textColor;

- (void)beginRefreshing:(UIScrollView *)scrollView;
- (void)stopRefreshing:(UIScrollView *)scrollView;
- (void)updateLastUpdateTime;
- (void)scrollViewDidScroll:(UIScrollView *)scrollView;
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView;

@end
