//
//  BFViewController.m
//  FeasycomWatch
//
//  Created by LIDONG on 14-2-28.
//  Copyright (c) 2014年 LIDONG. All rights reserved.
//

#import "BFViewController.h"
#import "BFActivityView.h"

@interface BFViewController () <BFActivityViewDelegate> {
    struct {
        unsigned int visible:1;
    } _extendedFlags;
}

@end

@implementation BFViewController

@dynamic isVisible;

static BFActivityView *_activityView = nil;

- (id)init {
    if (self = [super initWithNibName:nil bundle:nil]) {
        if ([self respondsToSelector:@selector(setEdgesForExtendedLayout:)]) {
            [self setEdgesForExtendedLayout:UIRectEdgeNone];
            [self setAutomaticallyAdjustsScrollViewInsets:NO];
        } else {
            [self setWantsFullScreenLayout:NO];
        }
    }
    return self;
}

- (void)setTitle:(NSString *)newTitle {
    [super setTitle:((newTitle && 0 < [newTitle length]) ? newTitle : nil)];
}

- (void)viewDidUnload {
    [super viewDidUnload];
    [self releaseUI];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
    if (!_extendedFlags.visible) {
        [self releaseUI];
        [self setView:nil];
    }
}

- (void)releaseUI {
    // Inherit this method to release UI
}

- (void)releaseOthers {
    // Inherit this method to release data or other objects
}

- (void)dealloc {
    [self releaseUI];
    [self releaseOthers];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [super dealloc];
}

- (void)viewWillAppear:(BOOL)animated {
    _extendedFlags.visible = YES;
}

- (void)viewDidDisappear:(BOOL)animated {
    _extendedFlags.visible = NO;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation {
    return (UIInterfaceOrientationPortrait == toInterfaceOrientation);
}

- (BOOL)shouldAutorotate {
    return NO;
}

//- (NSUInteger)supportedInterfaceOrientations {
//    return UIInterfaceOrientationMaskPortrait;
//}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation {
    return UIInterfaceOrientationPortrait;
}

- (BOOL)isVisible {
    return _extendedFlags.visible;
}

- (void)alertWithTitle:(NSString *)title message:(NSString *)message {
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title message:message delegate:nil cancelButtonTitle:LS(@"CLOSE") otherButtonTitles:nil];
	
	[alert show];
    [alert release];
}

- (void)alertWithTitle:(NSString *)title {
    [self alertWithTitle:title message:nil];
}

- (void)startWaitingWithTitle:(NSString *)title {
    if (nil == _activityView) {
        _activityView = [[BFActivityView alloc] init];
        [_activityView setDelegate:self];
    }
    [_activityView showWithTitle:title];
}

- (void)startWaiting {
    [self startWaitingWithTitle:NSStringEmpty];
}

- (void)stopWaiting {
    [_activityView dismiss];
}

- (void)stopWaitingWithTitle:(NSString *)title {
    if (nil == _activityView) {
        _activityView = [[BFActivityView alloc] init];
        [_activityView setDelegate:self];
    }
    [_activityView showWithTitle:title];
    [_activityView dismissWithTitle:title];
}

- (void)startWaitingWithoutBackground {
    if (nil == _activityView) {
        _activityView = [[BFActivityView alloc] init];
        [_activityView setDelegate:self];
    }
    [_activityView setHidesBackground:YES];
    [_activityView showWithTitle:nil];
}

#pragma mark - GTActivityViewDelegate

- (void)activityViewDidDismiss:(BFActivityView *)activityView {
    RELEASE_SAFELY(_activityView);
}

@end
