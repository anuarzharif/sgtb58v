//
//  BFViewController.h
//  FeasycomWatch
//
//  Created by LIDONG on 14-2-28.
//  Copyright (c) 2014年 LIDONG. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BFViewController : UIViewController

@property (nonatomic, assign, readonly) BOOL isVisible;

- (void)releaseUI;
- (void)releaseOthers;

- (void)alertWithTitle:(NSString *)title message:(NSString *)message;
- (void)alertWithTitle:(NSString *)title;

- (void)startWaiting;
- (void)startWaitingWithTitle:(NSString *)title;
- (void)stopWaiting;
- (void)stopWaitingWithTitle:(NSString *)title;
- (void)startWaitingWithoutBackground;

@end
