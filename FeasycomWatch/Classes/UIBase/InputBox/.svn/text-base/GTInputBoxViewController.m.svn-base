//
//  GTInputBoxViewController.m
//  DuoPai
//
//  Created by LIDONG on 13-12-30.
//  Copyright (c) 2013年 Jacky. All rights reserved.
//

#import "GTInputBoxViewController.h"
#import "GTDetectingView.h"

@interface GTInputBoxViewController () <GTDetectingViewDelegate> {
    GTInputBox *mInputBox;
    GTDetectingView *mDetectingView;
    int mStatusBarHeight;
    int mInputBarHeight;
    int mKeyboardHeight;
    BOOL mAutoHidesInputBox;
}

@end

@implementation GTInputBoxViewController

@synthesize autoHidesInputBox = mAutoHidesInputBox;

- (id)init {
    if (self = [super init]) {
        mStatusBarHeight = kDefaultStatusBarHeight;
        mInputBarHeight = kInputBarHeight;
        [self setPageHeight:(kScreenHeight - kDefaultStatusBarHeight - kNavBarHeight)];
        if ([self respondsToSelector:@selector(edgesForExtendedLayout)]) {
            [self setAutomaticallyAdjustsScrollViewInsets:NO];
        }
        [g_pNotificationCenter addObserver:self
                                  selector:@selector(statusBarFrameWillChangeNotification:)
                                      name:UIApplicationWillChangeStatusBarFrameNotification
                                    object:nil];
    }
    return self;
}

- (void)loadView {
    [super loadView];
    
    if (nil == mTableView) {
        const CGRect tableFrame = [self bounds];
        const CGRect inputFrame = { 0, tableFrame.origin.y + tableFrame.size.height, kScreenWidth, mInputBarHeight };
        
        mTableView = [[GTTableView alloc] initWithFrame:tableFrame style:UITableViewStylePlain];
        [mTableView setBackgroundColor:[UIColor whiteColor]];
        [mTableView setAutoresizesSubviews:NO];
        [mTableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
        
        mInputBox = [[GTInputBox alloc] initWithFrame:inputFrame];
        [mInputBox setDelegate:self];
    }
    [mWrapperView addSubview:mTableView];
    [mWrapperView addSubview:mInputBox];
}

- (void)dealloc {
    [mInputBox setDelegate:nil];
    [mDetectingView setDelegate:nil];
    [mTableView setDataSource:nil];
    [mTableView setDelegate:nil];
    
    [mTableView release];
    [mInputBox release];
    [mDetectingView release];
    
    [super dealloc];
}

- (void)scrollToIndexPath:(NSIndexPath *)indexPath {
    const CGRect rect = [mTableView rectForRowAtIndexPath:indexPath];
    CGPoint contentOffset = { 0, rect.origin.y + rect.size.height - mTableView.frame.size.height };
    
    if (contentOffset.y < - mKeyboardHeight) {
        contentOffset.y = - mKeyboardHeight;
    }
    
    [mTableView setContentOffset:contentOffset];
}

- (void)calculateLayout {
    CGRect tableFrame = [self bounds];
    
    tableFrame.origin.y -= mKeyboardHeight;
    tableFrame.size.height = (tableFrame.size.height - kDefaultStatusBarHeight + mStatusBarHeight);
    
    if (mAutoHidesInputBox) {
        if (0 < mKeyboardHeight) {
            tableFrame.origin.y -= mInputBarHeight;
        }
    } else {
        tableFrame.size.height -= mInputBarHeight;
    }
    const CGRect inputFrame = { 0, tableFrame.origin.y + tableFrame.size.height, kScreenWidth, mInputBarHeight + mKeyboardHeight };
    
    [mTableView setFrame:tableFrame];
    [mDetectingView setFrame:tableFrame];
    [mInputBox setFrame:inputFrame];
}

- (void)inputBoxWillShowUp {
    if (nil == mDetectingView) {
        CGRect tableFrame = { 0, 0, kScreenWidth, kScreenHeight - mStatusBarHeight - kNavBarHeight - mInputBarHeight };
        
        mDetectingView = [[GTDetectingView alloc] initWithFrame:tableFrame];
        [mDetectingView setDelegate:self];
    }
    [mWrapperView addSubview:mDetectingView];
}

- (void)inputBoxWillDrawBack {
    UIRELEASE(mDetectingView);
}

- (void)inputBoxShouldResizeByKeyboardHeight:(CGFloat)keyboardHeight inputBarHeight:(CGFloat)inputBarHeight {
    mKeyboardHeight = keyboardHeight;
    mInputBarHeight = inputBarHeight;
    
    [self calculateLayout];
}

- (void)inputBoxDidResize {
    /*
    if (0 < mKeyboardHeight) {
        CGPoint contentOffset = [mTableView contentOffset];
        CGRect tableFrame = { 0, 0, kScreenWidth, kScreenHeight - mStatusBarHeight - 44 - mInputBarHeight - mKeyboardHeight };
        
        contentOffset.y += mKeyboardHeight;
        
        [mTableView setFrame:tableFrame];
        [mTableView setContentOffset:contentOffset];
    }
     */
}

- (void)inputBoxDidFinishText:(NSString *)text {
    
}

#pragma mark - Action

- (void)beginEditingWithPlaceHolder:(NSString *)placeHolder {
    [mInputBox setPlaceHolder:placeHolder];
    [mInputBox becomeFirstResponder];
}

- (void)endEditing {
    [mInputBox resignFirstResponder];
}

- (void)activateInput:(BOOL)active {
    [mInputBox setUserInteractionEnabled:active];
    [mInputBox setExclusiveTouch:!active];
}

- (void)clearInputBox {
    [mInputBox clearContent];
}

#pragma mark - GTDetectingViewDelegate

- (void)detectingViewDidTouchDown:(GTDetectingView *)detectingView {
    [mInputBox resignFirstResponder];
}

#pragma mark - Notifications

- (void)statusBarFrameWillChangeNotification:(NSNotification *)notification {
    NSValue *statusBarFrame = [[notification userInfo] valueForKey:@"UIApplicationStatusBarFrameUserInfoKey"];
    
    if (statusBarFrame) {
        mStatusBarHeight = statusBarFrame.CGRectValue.size.height;
        [self calculateLayout];
    }
}


@end
