//
//  IBViewController.h
//  DuoPai
//
//  Created by LIDONG on 13-12-30.
//  Copyright (c) 2013年 LIDONG. All rights reserved.
//

#import "BFViewController.h"
#import "IBInputBox.h"
#import "IBDetectingView.h"

@interface IBViewController : BFViewController <UITableViewDataSource, UITableViewDelegate, IBInputBoxDelegate, IBDetectingViewDelegate> {
    UITableView *mTableView;
    IBInputBox *mInputBox;
    IBDetectingView *mDetectingView;
    int mStatusBarHeight;
    int mInputBarHeight;
    int mKeyboardHeight;
    BOOL mAutoHidesInputBox;
}

@property (nonatomic, assign) BOOL autoHidesInputBox;

- (void)clearInputBox;
- (void)beginEditingWithPlaceHolder:(NSString *)placeHolder;
- (void)endEditing;
- (void)scrollToIndexPath:(NSIndexPath *)indexPath;
- (void)activateInput:(BOOL)active;
- (void)deselect:(BOOL)animated;

@end
