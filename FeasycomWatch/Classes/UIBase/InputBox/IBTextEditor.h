//
//  IBTextEditor.h
//  LIDONG
//
//  Created by Li Dong on 12-3-6.
//  Copyright (c) 2012年 LIDONG. All rights reserved.
//

#import <UIKit/UIKit.h>

@class IBTextEditor;

@protocol IBTextEditorDelegate <NSObject>
@optional
- (BOOL)textEditorShouldBeginEditing:(IBTextEditor *)textEditor;
- (BOOL)textEditorShouldEndEditing:(IBTextEditor *)textEditor;
- (void)textEditorDidBeginEditing:(IBTextEditor *)textEditor;
- (void)textEditorDidEndEditing:(IBTextEditor *)textEditor;
- (BOOL)textEditor:(IBTextEditor *)textEditor shouldChangeTextInRange:(NSRange)range replacementText: (NSString *)replacementText;
- (void)textEditorDidChange:(IBTextEditor*)textEditor;
- (BOOL)textEditor:(IBTextEditor*)textEditor shouldResizeByHeight:(CGFloat)height;
@end


@interface IBTextEditor : UIView <UITextViewDelegate> {
    UIImage *mNormalBackgroundImage;
    UIImage *mHighlightedBackgroundImage;
    UITextView *mInternalTextView;
    UILabel *mPlaceHolderLabel;
    id<IBTextEditorDelegate> mDelegate;
    CGFloat mLineHeight;
    CGFloat mLastTextHeight;
    BOOL mEditing;
}

@property (nonatomic, retain) UIImage *backgroundImage;
@property (nonatomic, assign) id<IBTextEditorDelegate> delegate;

- (NSString *)text;
- (void)setText:(NSString *)newText;
- (void)insertText:(NSString *)text;
- (void)deleteBackward;

- (NSString *)placeHolder;
- (void)setPlaceHolder:(NSString *)newPlaceHolder;

@end
