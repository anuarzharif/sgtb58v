//
//  KKOtherInputView.h
//  LIDONG
//
//  Created by Li Dong on 12-2-21.
//  Copyright (c) 2012年 LIDONG. All rights reserved.
//

#import <UIKit/UIKit.h>

#define kInputBarHeight 49

typedef enum __GTEditingState {
    kEditingStyleNone,
    kEditingStyleSystemKeyboard
} GTEditingState;

@class IBInputBox;

@protocol IBInputBoxDelegate<NSObject>
@required
- (void)inputBoxWillShowUp;
- (void)inputBoxWillDrawBack;
- (void)inputBoxShouldResizeByKeyboardHeight:(CGFloat)keyboardHeight inputBarHeight:(CGFloat)inputBarHieght;
- (void)inputBoxDidResize;
- (void)inputBoxDidFinishText:(NSString *)text;
@end

@interface IBInputBox : UIView {
}

@property (nonatomic, assign) id<IBInputBoxDelegate> delegate;

- (void)insertText:(NSString *)text;
- (void)clearContent;
- (void)setPlaceHolder:(NSString *)placeHolder;

@end
