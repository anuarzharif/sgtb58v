//
//  IBDetectingView.m
//  DuoPai
//
//  Created by LIDONG on 14-1-6.
//  Copyright (c) 2014年 LIDONG. All rights reserved.
//

#import "IBDetectingView.h"

@implementation IBDetectingView

@synthesize delegate = _delegate;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setBackgroundColor:[UIColor clearColor]];
        [self setUserInteractionEnabled:YES];
        [self setExclusiveTouch:YES];
    }
    return self;
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [_delegate detectingViewDidTouchDown:self];
}

@end
