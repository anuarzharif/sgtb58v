//
//  KKOtherInputView.m
//  LIDONG
//
//  Created by Li Dong on 12-2-21.
//  Copyright (c) 2012年 LIDONG. All rights reserved.
//

#import "IBInputBox.h"
#import "IBTextEditor.h"


#define kEditorPaddingY 4


@interface IBInputBox () <IBTextEditorDelegate> {
    IBTextEditor *mTextEditor;
    UIImageView *mBarBackgroundView;
    NSCharacterSet *mNewlineCharacterSet;
    GTEditingState mEditingState;
    int mEditorHeight;
    int mSystemKeyboardHeight;
    int mKeyboardHeight;
    int mInputBarHeight;
    id<IBInputBoxDelegate> mDelegate;
}

- (void)commonKeyboardShowUp;

@end


@implementation IBInputBox

@synthesize delegate = mDelegate;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setBackgroundColor:[UIColor grayColor]];
        [self setAutoresizesSubviews:NO];
        [self setAutoresizingMask:UIViewAutoresizingNone];
        
        mBarBackgroundView = [[UIImageView alloc] initWithFrame:CGRectZero];
        [mBarBackgroundView setImage:[UIImage imageNamed:@"inputBackground.png"]];
        [self addSubview:mBarBackgroundView];
        
        mTextEditor = [[IBTextEditor alloc] initWithFrame:CGRectZero];
        [mTextEditor setDelegate:self];
        [self addSubview:mTextEditor];
        
        mNewlineCharacterSet = [[NSCharacterSet newlineCharacterSet] retain];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(keyboardFrameWillChange:)
                                                     name:UIKeyboardWillChangeFrameNotification
                                                   object:nil];
        mEditingState = kEditingStyleNone;
        mSystemKeyboardHeight = kDefaultKeyboardHeight;
    }
    return self;
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    [mBarBackgroundView release];
    [mTextEditor release];
    [mNewlineCharacterSet release];
    
    [super dealloc];
}

- (void)layoutSubviews {
    const CGRect barFrame = { 0, 0, kScreenWidth, (mEditorHeight + 12) };
    const CGRect editorFrame = { 5, 6, 310, mEditorHeight };
    
    [mBarBackgroundView setFrame:barFrame];
    [mTextEditor setFrame:editorFrame];
}

#pragma mark - IBTextEditorDelegate

- (void)resizeAnimated:(BOOL)animated {
    int newInputBarHeight = mEditorHeight + 12;
    int newKeyboardHeight = 0;
    
    if (kEditingStyleSystemKeyboard == mEditingState) {
        newKeyboardHeight += mSystemKeyboardHeight;
    }
    if (newInputBarHeight != mInputBarHeight || newKeyboardHeight != mKeyboardHeight) {
        mInputBarHeight = newInputBarHeight;
        mKeyboardHeight = newKeyboardHeight;
        if (animated) {
            [UIView animateWithDuration:0.25 animations:^{
                [mDelegate inputBoxShouldResizeByKeyboardHeight:mKeyboardHeight inputBarHeight:mInputBarHeight];
            } completion:^(BOOL finished){
                [mDelegate inputBoxDidResize];
            }];
        } else {
            [mDelegate inputBoxShouldResizeByKeyboardHeight:mKeyboardHeight inputBarHeight:mInputBarHeight];
            [mDelegate inputBoxDidResize];
        }
    }
}

- (void)setEditingState:(const GTEditingState)newState {
    if (newState != mEditingState) {
        const GTEditingState oldState = mEditingState;
        BOOL animated = NO;
        
        mEditingState = newState;
        
        if (kEditingStyleNone == oldState) {
            animated = YES;
            [mDelegate inputBoxWillShowUp];
        }
        if (kEditingStyleNone == newState) {
            animated = YES;
            [mDelegate inputBoxWillDrawBack];
        }
        
        [self resizeAnimated:animated];
    }
}

- (void)sendText {
    NSString *text = [mTextEditor text];
    
    if (text && ![text isEqualToString:NSStringEmpty]) {
        [mDelegate inputBoxDidFinishText:text];
        [mTextEditor setText:NSStringEmpty];
    }
}

- (void)textEditorDidBeginEditing:(IBTextEditor *)textEditor {
    [self setEditingState:kEditingStyleSystemKeyboard];
}

- (void)textEditorDidEndEditing:(IBTextEditor *)textEditor {
}

- (BOOL)textEditor:(IBTextEditor *)textEditor shouldChangeTextInRange:(NSRange)range replacementText: (NSString *)replacementText {
    const NSRange newlineRange = [replacementText rangeOfCharacterFromSet:mNewlineCharacterSet];
    
    if (NSNotFound == newlineRange.location) {
        return YES;
    }
    if (0 == newlineRange.location && 2 >= newlineRange.length) {
        [self sendText];
    }
    return NO;
}

- (void)textEditorDidChange:(IBTextEditor*)textEditor {
}

- (BOOL)textEditor:(IBTextEditor *)textEditor shouldResizeByHeight:(CGFloat)height {
    mEditorHeight = height;
    [self resizeAnimated:NO];
    return YES;
}

#pragma mark - Action Methods

- (void)commonKeyboardShowUp {
    [mTextEditor becomeFirstResponder];
}

- (void)setPlaceHolder:(NSString *)placeHolder {
    [mTextEditor setText:nil];
    [mTextEditor setPlaceHolder:placeHolder];
}

- (void)insertText:(NSString *)text {
    [mTextEditor insertText:text];
}

- (void)clearContent {
    [mTextEditor setPlaceHolder:nil];
    [mTextEditor setText:nil];
}

- (BOOL)canBecomeFirstResponder {
    return [mTextEditor canBecomeFirstResponder];
}

- (BOOL)becomeFirstResponder {
    return [mTextEditor becomeFirstResponder];
}

- (BOOL)canResignFirstResponder {
    return [mTextEditor canResignFirstResponder];
}

- (BOOL)resignFirstResponder {
    if ([mTextEditor isFirstResponder]) {
        [mTextEditor resignFirstResponder];
    }
    [self setEditingState:kEditingStyleNone];
    return YES;
}

- (BOOL)isFirstResponder {
    return [mTextEditor isFirstResponder];
}

#pragma mark - Notification

- (void)keyboardFrameWillChange:(NSNotification *)notification {
    NSNumber *keyboardBounds = [[notification userInfo] valueForKey:@"UIKeyboardBoundsUserInfoKey"];
    CGRect rect = [keyboardBounds CGRectValue];
    
    mSystemKeyboardHeight = rect.size.height;
    [self resizeAnimated:NO];
}

@end
