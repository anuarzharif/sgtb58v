//
//  IBDetectingView.h
//  DuoPai
//
//  Created by LIDONG on 14-1-6.
//  Copyright (c) 2014年 LIDONG. All rights reserved.
//

#import <UIKit/UIKit.h>

@class IBDetectingView;

@protocol IBDetectingViewDelegate <NSObject>
@required
- (void)detectingViewDidTouchDown:(IBDetectingView *)detectingView;
@end

@interface IBDetectingView : UIView

@property (nonatomic, assign) id<IBDetectingViewDelegate> delegate;

@end
