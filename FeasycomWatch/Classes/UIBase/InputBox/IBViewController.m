//
//  IBViewController.m
//  DuoPai
//
//  Created by LIDONG on 13-12-30.
//  Copyright (c) 2013年 LIDONG. All rights reserved.
//

#import "IBViewController.h"

@implementation IBViewController

@synthesize autoHidesInputBox = mAutoHidesInputBox;

- (id)init {
    if (self = [super init]) {
        mStatusBarHeight = kDefaultStatusBarHeight;
        mInputBarHeight = kInputBarHeight;
        [self setHidesBottomBarWhenPushed:YES];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(statusBarFrameWillChangeNotification:)
                                                     name:UIApplicationWillChangeStatusBarFrameNotification
                                                   object:nil];
    }
    return self;
}

- (void)loadView {
    [super loadView];
    
    UIView *view = [self view];
    
    if (nil == mTableView) {
        const CGRect bounds = [view bounds];
        const CGRect tableFrame = { 0, kNavBarHeight, bounds.size.width, bounds.size.height - kNavBarHeight };
        const CGRect inputFrame = { 0, tableFrame.origin.y + tableFrame.size.height, kScreenWidth, mInputBarHeight };
        
        mTableView = [[UITableView alloc] initWithFrame:tableFrame style:UITableViewStylePlain];
        [mTableView setDataSource:self];
        [mTableView setDelegate:self];
        [mTableView setBackgroundColor:[UIColor whiteColor]];
        [mTableView setAutoresizesSubviews:NO];
        [mTableView setSeparatorStyle:UITableViewCellSeparatorStyleSingleLine];
        
        mInputBox = [[IBInputBox alloc] initWithFrame:inputFrame];
        [mInputBox setDelegate:self];
    }
    [view addSubview:mTableView];
    [view addSubview:mInputBox];
}

- (void)releaseUI {
    [mInputBox setDelegate:nil];
    [mDetectingView setDelegate:nil];
    [mTableView setDataSource:nil];
    [mTableView setDelegate:nil];
    
    RELEASE_SAFELY(mTableView);
    RELEASE_SAFELY(mInputBox);
    RELEASE_SAFELY(mDetectingView);
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    return nil;
}

- (void)scrollToIndexPath:(NSIndexPath *)indexPath {
    const CGRect rect = [mTableView rectForRowAtIndexPath:indexPath];
    CGPoint contentOffset = { 0, rect.origin.y + rect.size.height - mTableView.frame.size.height };
    
    if (contentOffset.y < - mKeyboardHeight) {
        contentOffset.y = - mKeyboardHeight;
    }
    
    [mTableView setContentOffset:contentOffset];
}

- (void)calculateLayout {
    const CGRect bounds = [self.view bounds];
    CGRect tableFrame = { 0, kNavBarHeight, bounds.size.width, bounds.size.height - kNavBarHeight };
    
    tableFrame.origin.y -= mKeyboardHeight;
    tableFrame.size.height = (tableFrame.size.height - kDefaultStatusBarHeight + mStatusBarHeight);
    
    if (mAutoHidesInputBox) {
        if (0 < mKeyboardHeight) {
            tableFrame.origin.y -= mInputBarHeight;
        }
    } else {
        tableFrame.size.height -= mInputBarHeight;
    }
    const CGRect inputFrame = { 0, tableFrame.origin.y + tableFrame.size.height, kScreenWidth, mInputBarHeight + mKeyboardHeight };
    
    [mTableView setFrame:tableFrame];
    [mDetectingView setFrame:tableFrame];
    [mInputBox setFrame:inputFrame];
}

- (void)inputBoxWillShowUp {
    if (nil == mDetectingView) {
        CGRect tableFrame = { 0, 0, kScreenWidth, kScreenHeight - mStatusBarHeight - kNavBarHeight - mInputBarHeight };
        
        mDetectingView = [[IBDetectingView alloc] initWithFrame:tableFrame];
        [mDetectingView setDelegate:self];
    }
    [[self view] addSubview:mDetectingView];
}

- (void)inputBoxWillDrawBack {
    UIVIEW_RELEASE_SAFELY(mDetectingView);
}

- (void)inputBoxShouldResizeByKeyboardHeight:(CGFloat)keyboardHeight inputBarHeight:(CGFloat)inputBarHeight {
    mKeyboardHeight = keyboardHeight;
    mInputBarHeight = inputBarHeight;
    
    [self calculateLayout];
}

- (void)inputBoxDidResize {
    /*
    if (0 < mKeyboardHeight) {
        CGPoint contentOffset = [mTableView contentOffset];
        CGRect tableFrame = { 0, 0, kScreenWidth, kScreenHeight - mStatusBarHeight - 44 - mInputBarHeight - mKeyboardHeight };
        
        contentOffset.y += mKeyboardHeight;
        
        [mTableView setFrame:tableFrame];
        [mTableView setContentOffset:contentOffset];
    }
     */
}

- (void)inputBoxDidFinishText:(NSString *)text {
    
}

- (void)deselect:(BOOL)animated {
    NSIndexPath *selectedIndexPath = [mTableView indexPathForSelectedRow];
    
    if (selectedIndexPath) {
        [mTableView deselectRowAtIndexPath:selectedIndexPath animated:animated];
    }
}

#pragma mark - Action

- (void)beginEditingWithPlaceHolder:(NSString *)placeHolder {
    [mInputBox setPlaceHolder:placeHolder];
    [mInputBox becomeFirstResponder];
}

- (void)endEditing {
    [mInputBox resignFirstResponder];
}

- (void)activateInput:(BOOL)active {
    [mInputBox setUserInteractionEnabled:active];
    [mInputBox setExclusiveTouch:!active];
}

- (void)clearInputBox {
    [mInputBox clearContent];
}

#pragma mark - IBDetectingViewDelegate

- (void)detectingViewDidTouchDown:(IBDetectingView *)detectingView {
    [mInputBox resignFirstResponder];
}

#pragma mark - Notifications

- (void)statusBarFrameWillChangeNotification:(NSNotification *)notification {
    NSValue *statusBarFrame = [[notification userInfo] valueForKey:@"UIApplicationStatusBarFrameUserInfoKey"];
    
    if (statusBarFrame) {
        mStatusBarHeight = statusBarFrame.CGRectValue.size.height;
        [self calculateLayout];
    }
}


@end
