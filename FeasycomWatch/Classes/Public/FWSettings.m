//
//  FWSettings.m
//  FeasycomWatch
//
//  Created by LIDONG on 14-2-24.
//  Copyright (c) 2014年 LIDONG. All rights reserved.
//

#import "FWSettings.h"


NSString * const NSStringEmpty = @"";

CGRect FWScreenBounds;
CGFloat FWScreenScale;


void FWGlobalIntialize(void) {
    UIScreen *mainScreen = [UIScreen mainScreen];
    
    FWScreenBounds = [mainScreen bounds];
    FWScreenScale = [mainScreen scale];
}