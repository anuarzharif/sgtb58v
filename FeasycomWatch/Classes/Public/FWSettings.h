//
//  FWSettings.h
//  FeasycomWatch
//
//  Created by LIDONG on 14-2-24.
//  Copyright (c) 2014年 LIDONG. All rights reserved.
//

#import <Foundation/Foundation.h>


#define kScreenWidth            FWScreenBounds.size.width
#define kScreenHeight           FWScreenBounds.size.height
#define kDefaultStatusBarHeight 20
#define kDefaultKeyboardHeight  216
#define kNavBarHeight           44
#define kTabBarHeight           49
#define kFullViewWidth          kScreenWidth
#define kFullViewHeight         (kScreenHeight - kNavBarHeight - kDefaultStatusBarHeight)
#define kMainViewHeight         (kFullViewHeight - kTabBarHeight)

#define UIViewAutoresizingFlexibleHorizontalMargin (UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin)
#define UIViewAutoresizingFlexibleVerticalMargin (UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin)
#define UIViewAutoresizingFlexibleAllMargin (UIViewAutoresizingFlexibleHorizontalMargin | UIViewAutoresizingFlexibleVerticalMargin)
#define UIViewAutoresizingFlexibleSize (UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight)


extern NSString * const NSStringEmpty;

extern CGRect FWScreenBounds;
extern CGFloat FWScreenScale;