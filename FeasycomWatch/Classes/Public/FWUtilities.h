//
//  FWUtilities.h
//  FeasycomWatch
//
//  Created by LIDONG on 14-2-24.
//  Copyright (c) 2014年 LIDONG. All rights reserved.
//

#import <UIKit/UIKit.h>

//////////////////////////////////////////////////////////////////////////////
// C函数

#ifdef __cplusplus
extern "C" {
#endif
    
    extern NSTimeInterval NSGetCurrentTimestamp(void);
    extern NSString *NSDescriptionForCurrentTime(void);
    extern NSString *NSShortDescriptionForCurrentTime(void);
    extern NSMutableArray *NSArrayCreateMutableWithCapacity(const CFIndex capacity);
    
#ifdef __cplusplus
}
#endif


//////////////////////////////////////////////////////////////////////////////
// 内存管理

#define RETAIN_SAFELY(receiver, assigner) do { [receiver release]; receiver = [assigner retain]; } while (0)
#define COPY_SAFELY(receiver, assigner) do { [receiver release]; receiver = [assigner copy]; } while (0)
#define RELEASE_SAFELY(obj) do { [obj release]; obj = nil; } while (0)
#define UIVIEW_RELEASE_SAFELY(view) do { [view removeFromSuperview]; [view release]; view = nil; } while(0)


//////////////////////////////////////////////////////////////////////////////
// 多语言
#define LS(key) NSLocalizedString(key, nil)


//////////////////////////////////////////////////////////////////////////////
// 其他
#define STOP_TIMER(timer) do { [timer invalidate]; timer = nil; } while(0)

#define ALERT(title, messageText) do { \
UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title message:messageText delegate:nil cancelButtonTitle:LS(@"CLOSE") otherButtonTitles:nil]; \
[alert show]; \
[alert release]; } while (0)
