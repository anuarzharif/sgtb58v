//
//  DLog.h
//  FeasycomWatch
//
//  Created by LIDONG on 14-3-2.
//  Copyright (c) 2014年 LIDONG. All rights reserved.
//

#ifndef __DLOG_H__
#define __DLOG_H__

#import "FWUtilities.h"

//////////////////////////////////////////////////////////////////////////////
// 日志

#if ENABLE_LOG && DEBUG

#define DLog(FORMAT, ...) printf("[%s] %s\n", [NSDescriptionForCurrentTime() UTF8String], [[NSString stringWithFormat:FORMAT, ##__VA_ARGS__] UTF8String])
#define DLogTrace() DLog(@"[TRACING] METHOD: %s >> LINE: %d", __PRETTY_FUNCTION__, __LINE__)
#define DLogError(FORMAT, ...) DLog([NSString stringWithFormat:@"[ERROR] FILE: %@ >> LINE: %d %@", THIS_FILE, __LINE__, FORMAT], ##__VA_ARGS__)
#define DLogError0() DLog(@"[ERROR] FILE: %@ >> LINE: %d", THIS_FILE, __LINE__)

#else

#define DLog(FORMAT, ...)
#define DLogTrace()
#define DLogError(FORMAT, ...)
#define DLogError0()

#endif // ENABLE_LOG && DEBUG


#endif // __DLOG_H__
