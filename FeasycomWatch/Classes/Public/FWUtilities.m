//
//  FWUtilities.m
//  FeasycomWatch
//
//  Created by LIDONG on 14-2-24.
//  Copyright (c) 2014年 LIDONG. All rights reserved.
//

#import "FWUtilities.h"
#import <sys/time.h>

NSTimeInterval NSGetCurrentTimestamp(void) {
    struct timeval tv = { 0 };
    
    gettimeofday(&tv, 0);
    return (double)tv.tv_sec + (double)tv.tv_usec / 1000000.f;
}

NSString *NSDescriptionForCurrentTime(void) {
    struct timeval tv = { 0 };
    
    gettimeofday(&tv, 0);
    
    const UInt64 milliseconds = (UInt64)tv.tv_sec * 1000ULL + (UInt64)((tv.tv_usec + 500) / 1000);
    const time_t t = (const time_t)(milliseconds / 1000ULL);
    const struct tm *ptm = localtime(&t);
    
    return [NSString stringWithFormat:@"%04d-%02d-%02d %02d:%02d:%02d", (1900 + ptm->tm_year), (ptm->tm_mon + 1), ptm->tm_mday, ptm->tm_hour, ptm->tm_min, ptm->tm_sec];
}

NSString *NSShortDescriptionForCurrentTime(void) {
    struct timeval tv = { 0 };
    
    gettimeofday(&tv, 0);
    
    const UInt64 milliseconds = (UInt64)tv.tv_sec * 1000ULL + (UInt64)((tv.tv_usec + 500) / 1000);
    const time_t t = (const time_t)(milliseconds / 1000ULL);
    const struct tm *ptm = localtime(&t);
    
    return [NSString stringWithFormat:@"%d-%d %d:%02d:%02d", (ptm->tm_mon + 1), ptm->tm_mday, ptm->tm_hour, ptm->tm_min, ptm->tm_sec];
}

NSMutableArray *NSArrayCreateMutableWithCapacity(const CFIndex capacity) {
    const CFArrayCallBacks callbacks = { 0, NULL, NULL, NULL, NULL };
    
    return (NSMutableArray *)CFArrayCreateMutable(kCFAllocatorDefault, capacity, &callbacks);
}
