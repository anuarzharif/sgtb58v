//
//  CommentCell.m
//  LIDONG
//
//  Created by LiDong on 13-6-25.
//  Copyright (c) 2013年 LIDONG. All rights reserved.
//

#import "FWMessageCell.h"
#import "FWMessage.h"
#import <QuartzCore/QuartzCore.h>

@implementation FWMessageCell

@synthesize message = mMessage;

- (id)initWithReuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier];
    if (self) {
        UIColor *clearColor = [UIColor clearColor];
        UIColor *textColor = [UIColor darkTextColor];
        UIView *contentView = [self contentView];
        
        mNameLabel = [[UILabel alloc] initWithFrame:CGRectMake(5, 5, 180, 20)];
        [mNameLabel setBackgroundColor:clearColor];
        [mNameLabel setTextColor:textColor];
        [mNameLabel setTextAlignment:NSTextAlignmentLeft];
        [mNameLabel setFont:[UIFont boldSystemFontOfSize:14]];
        [contentView addSubview:mNameLabel];
        
        mTimeLabel = [[UILabel alloc] initWithFrame:CGRectMake(185, 5, 130, 20)];
        [mTimeLabel setBackgroundColor:clearColor];
        [mTimeLabel setTextColor:[UIColor grayColor]];
        [mTimeLabel setTextAlignment:NSTextAlignmentRight];
        [mTimeLabel setFont:[UIFont systemFontOfSize:12]];
        [contentView addSubview:mTimeLabel];
        
        mContentLabel = [[UILabel alloc] initWithFrame:CGRectMake(5, 30, 310, 20)];
        [mContentLabel setBackgroundColor:clearColor];
        [mContentLabel setTextColor:textColor];
        [mContentLabel setFont:[UIFont systemFontOfSize:14]];
        [mContentLabel setNumberOfLines:0];
        [contentView addSubview:mContentLabel];
        
        [self setSelectionStyle:UITableViewCellSelectionStyleGray];
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    [mContentLabel setFrame:CGRectMake(5, 30, 310, [mMessage height])];
}

- (void)dealloc {
    [mNameLabel release];
    [mContentLabel release];
    [mTimeLabel release];
    [mMessage release];
    
    [super dealloc];
}

- (void)setMessage:(FWMessage *)message {
    if (message != mMessage) {
        RETAIN_SAFELY(mMessage, message);
        [mNameLabel setText:[mMessage name]];
        [mTimeLabel setText:[NSString stringWithFormat:@"%dB | %@", (int)[mMessage length], [mMessage time]]];
        [mContentLabel setFrame:CGRectMake(44, 25, 260, [mMessage height])];
        [mContentLabel setText:[mMessage content]];
    }
}

@end
