//
//  CommentCell.h
//  LIDONG
//
//  Created by LiDong on 13-6-25.
//  Copyright (c) 2013年 LIDONG. All rights reserved.
//

#import <UIKit/UIKit.h>

@class FWMessage;

@interface FWMessageCell : UITableViewCell {
    UILabel *mNameLabel;
    UILabel *mTimeLabel;
    UILabel *mContentLabel;
    FWMessage *mMessage;
}

@property (nonatomic, retain) FWMessage *message;

- (id)initWithReuseIdentifier:(NSString *)reuseIdentifier;

@end
