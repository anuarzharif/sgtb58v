//
//  FWAppDelegate.h
//  FeasycomWatch
//
//  Created by LIDONG on 14-2-23.
//  Copyright (c) 2014年 LIDONG. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FWAppDelegate : UIResponder <UIApplicationDelegate> {
    UIWindow *mWindow;
}

@property (retain, nonatomic) UIWindow *window;

@end
