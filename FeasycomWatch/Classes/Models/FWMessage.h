//
//  FWMessage.h
//  FeasycomWatch
//
//  Created by LIDONG on 14-3-1.
//  Copyright (c) 2014年 LIDONG. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FWMessage : NSObject

@property (nonatomic, retain, readonly) NSString *name;
@property (nonatomic, retain, readonly) NSString *time;
@property (nonatomic, retain, readonly) NSString *content;
@property (nonatomic, assign, readonly) NSUInteger length;
@property (nonatomic, assign, readonly) CGFloat height;

- (id)initWithData:(NSData *)data name:(NSString *)name;

@end
