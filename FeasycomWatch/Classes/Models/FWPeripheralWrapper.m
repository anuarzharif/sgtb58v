//
//  FWPeripheralWrapper.m
//  FeasycomWatch
//
//  Created by LIDONG on 14-2-28.
//  Copyright (c) 2014年 LIDONG. All rights reserved.
//

#import "FWPeripheralWrapper.h"
#import <CoreBluetooth/CoreBluetooth.h>

@interface FWPeripheralWrapper () {
    CBPeripheral *mPeripheral;
    NSString *mUUIDString;
    NSInteger mRSSI;
    NSString *mAdvertisementData;
}

@end

@implementation FWPeripheralWrapper

@synthesize peripheral = mPeripheral;
@synthesize UUIDString = mUUIDString;
@synthesize RSSI = mRSSI;
@synthesize advertisementData = mAdvertisementData;

- (id)initWithPeripheral:(CBPeripheral *)peripheral RSSI:(NSNumber *)RSSI advertisementData:(NSDictionary *)advertisementData {
    if (self = [super init]) {
        mPeripheral = [peripheral retain];
        
        if ([peripheral respondsToSelector:@selector(identifier)]) {
            mUUIDString = [[[peripheral identifier] UUIDString] retain];
        } else if ([peripheral respondsToSelector:@selector(UUID)]) {
//            mUUIDString = (NSString *)CFUUIDCreateString(kCFAllocatorDefault, [peripheral identifier]);
            
            //temporary
            //not sure yet what is doing here
            NSString *strgTemp = [NSString stringWithFormat:@"%@", [peripheral identifier].UUIDString];
            mUUIDString = strgTemp;
        }
        mRSSI = [RSSI integerValue];
        
        NSArray *uuids = [advertisementData objectForKey:@"kCBAdvDataServiceUUIDs"];
        
        mAdvertisementData = [[NSString alloc] initWithFormat:@"Channel: %d Connectable: %@\nUUIDs: %@", [[advertisementData objectForKey:@"kCBAdvDataChannel"] intValue], [[advertisementData objectForKey:@"kCBAdvDataIsConnectable"] boolValue] ? @"YES" : @"NO", [uuids componentsJoinedByString:@", "]];
    }
    return self;
}

- (void)dealloc {
    [mPeripheral release];
    [mUUIDString release];
    [mAdvertisementData release];
    [super dealloc];
}

- (BOOL)isEqual:(FWPeripheralWrapper *)otherObject {
    return [mUUIDString isEqualToString:otherObject->mUUIDString];
}

- (NSString *)name {
    return [mPeripheral name];
}

@end
