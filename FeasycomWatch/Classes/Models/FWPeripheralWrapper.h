//
//  FWPeripheralWrapper.h
//  FeasycomWatch
//
//  Created by LIDONG on 14-2-28.
//  Copyright (c) 2014年 LIDONG. All rights reserved.
//

#import <Foundation/Foundation.h>

@class CBPeripheral;

@interface FWPeripheralWrapper : NSObject

@property (nonatomic, retain, readonly) CBPeripheral *peripheral;
@property (nonatomic, retain, readonly) NSString *UUIDString;
@property (nonatomic, assign, readonly) NSInteger RSSI;
@property (nonatomic, retain, readonly) NSString *advertisementData;

- (id)initWithPeripheral:(CBPeripheral *)peripheral RSSI:(NSNumber *)RSSI advertisementData:(NSDictionary *)advertisementData;
- (NSString *)name;

@end
