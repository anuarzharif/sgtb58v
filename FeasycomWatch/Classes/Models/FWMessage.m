//
//  FWMessage.m
//  FeasycomWatch
//
//  Created by LIDONG on 14-3-1.
//  Copyright (c) 2014年 LIDONG. All rights reserved.
//

#import "FWMessage.h"

@implementation FWMessage

@synthesize name = _name;
@synthesize content = _content;
@synthesize time = _time;
@synthesize length = _length;
@synthesize height = _height;

- (id)initWithData:(NSData *)data name:(NSString *)name {
    if (self = [super init]) {
        _content = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        _length = [data length];
        _name = [name retain];
        _time = [NSShortDescriptionForCurrentTime() retain];
    }
    return self;
}

- (void)dealloc {
    [_name release];
    [_content release];
    [_time release];
    [super dealloc];
}

- (CGFloat)height {
    if (1.f > _height) {
        _height = [_content sizeWithFont:[UIFont systemFontOfSize:14] constrainedToSize:CGSizeMake(310, CGFLOAT_MAX) lineBreakMode:NSLineBreakByWordWrapping].height;
    }
    return _height;
}

@end
