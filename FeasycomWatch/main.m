//
//  main.m
//  FeasycomWatch
//
//  Created by LIDONG on 14-2-23.
//  Copyright (c) 2014年 LIDONG. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "FWAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([FWAppDelegate class]));
    }
}
